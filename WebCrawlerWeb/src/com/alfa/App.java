package com.alfa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import com.alfa.InvertedIndex.InvInd;
import com.alfa.webcrawler.WebCrawler;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class App {
	public static int pageCount = -1;
	public static int maxPageDownload = 1000;

	public static void main(String[] args) {
/**
 *shouldCrawl = boolean = Weather the program should craw the web or not
 *							if false will try to use the serialized file given by-> serializedCrawlFile
 *							else will crawl the web and serialize with the same file name
 *shouldGenerateInvInd = boolean = Weather the program should generate the inverted index or not
 *							if false will try to use the serialized file given by-> serializedInvertedIndFile
 *							else will generate the inverted index and serialize with the same file name
 *startURL = String = where to start the crawling process if crawling is done
 *basePath = String = Storage directory for the crawled , parsed and processed pages
 *query = String = Query meant to be searched  
 */
		// configureProxy();

		boolean shouldCrawl = false;
		boolean shouldGenerateInvInd = false;
		String serializeCrawlFile = "Crawlit.ser";
		String serializeInvertedIndFile = "InvertedIndexKryo.ser";

		String startURL = "https://en.wikipedia.org/wiki/Mahatma_Gandhi";
		String basePath = "E:\\CrawlData\\";
		String query = "intend category gandhi";

		WebCrawler wCrawl = null;
		if (shouldCrawl) {
			File f = new File(basePath);
			if (!f.exists()) {
				f.mkdirs();
			}
			File[] existingFiles = f.listFiles();
			if (existingFiles.length > 0) {
				System.out.println("Files Already Exists in crawiling destination. Delete all ? (Y/N)");
				Scanner sc = new Scanner(System.in);
				if (sc.next().equals("Y") || sc.next().equals("y")) {
					System.out.println("Removing all files from scraping destination.");
					for (File file : existingFiles) {
						file.delete();
					}
				} else {
					if (sc.next().equals("n") || sc.next().equals("N")) {
						System.out.println("Exiting...");
					} else {
						System.out.println("Invalid input. Exiting...");
					}
				}
				sc.close();
			}

			wCrawl = new WebCrawler();
			wCrawl.processPage(startURL, basePath);
			serializeCrawl(serializeCrawlFile, wCrawl);
		} else {
			wCrawl = deserializeCrawl(serializeCrawlFile);
		}

		Map<String, String> urlToPageMapping = wCrawl.urlToPageMapping;

		InvInd invertedIndex = new InvInd();
		if (shouldGenerateInvInd) {
			try {

				invertedIndex.readWordFile(basePath);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			invertedIndex.generateInvertedIndex();
			
			serializeInvertedIndexKryo(serializeInvertedIndFile, invertedIndex);
		} else {
			invertedIndex = deserializeInvertedIndexKryo(serializeInvertedIndFile);
		}

		Map<String, Double> fileRank = invertedIndex.search(query);

		printResults(query, urlToPageMapping, fileRank);

		query = "Mohandas Karamchand Gandhi";
		Map<String, Double> fileRank2 = invertedIndex.search(query);

		printResults(query, urlToPageMapping, fileRank2);
		
	}

	private static void printResults(String query, Map<String, String> urlToPageMapping, Map<String, Double> fileRank) {
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("\"" + query + "\" Found in :");
		for (Entry<String, Double> fr : fileRank.entrySet()) {
			String doc = fr.getKey();
			Double freq = fr.getValue();
			System.out
					.println(freq + " " + doc + "  " + urlToPageMapping.get(doc.substring(doc.lastIndexOf('\\') + 1)));
		}
		System.out.println("-----------------------------------------------------------------------");
	}

	static boolean serializeCrawl(String filename, WebCrawler wc) {
		System.out.println("Serializing Crawl data....");
		// Serialization
		try (Output output = new Output(new FileOutputStream(filename))) {
			Kryo kryo = new Kryo();
			kryo.writeClassAndObject(output, wc);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			return false;
		}
		System.out.println("Serialized...");
		return true;
	}

	static WebCrawler deserializeCrawl(String filename) {
		System.out.println("Deserializing Crawled Data...");
		WebCrawler wc = null;
		try (Input input = new Input(new FileInputStream(filename))) {
			Kryo kryo = new Kryo();
			wc = (WebCrawler) kryo.readClassAndObject(input);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		System.out.println("Deserailized...");
		return wc;

	}

	static boolean serializeInvertedIndexKryo(String filename, InvInd ii) {
		System.out.println("Serializing Inverted Index...");
		try (Output output = new Output(new FileOutputStream(filename))) {
			Kryo kryo = new Kryo();
			kryo.writeClassAndObject(output, ii);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			return false;
		}
		System.out.println("Serialized...");
		return true;
	}

	static InvInd deserializeInvertedIndexKryo(String filename) {
		System.out.println("Deserializing Inverted Index...");
		InvInd ii = null;
		try (Input input = new Input(new FileInputStream(filename))) {
			Kryo kryo = new Kryo();
			ii = (InvInd) kryo.readClassAndObject(input);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}

		System.out.println("Deserialization Complete...");
		return ii;

	}

	@SuppressWarnings("unused")
	private static void configureProxy() {
		String host = "172.25.74.12";
		String port = "8080";
		String userName = "";
		String password = "";
		System.out.println("Using proxy: " + host + ":" + port);
		System.getProperties().put("http.proxyHost", host);
		System.getProperties().put("http.proxyPort", port);
		System.getProperties().put("http.proxyUser", userName);
		System.getProperties().put("http.proxyPassword", password);
		System.getProperties().put("http.nonProxyHosts", "localhost|127.0.0.1");
		System.getProperties().put("https.proxyHost", host);
		System.getProperties().put("https.proxyPort", port);
		System.getProperties().put("https.proxyUser", userName);
		System.getProperties().put("https.proxyPassword", password);
		System.getProperties().put("https.nonProxyHosts", "localhost|127.0.0.1");
	}
}

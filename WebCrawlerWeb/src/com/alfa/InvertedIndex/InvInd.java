package com.alfa.InvertedIndex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import com.alfa.cosine.CosineSimmilarity;

public class InvInd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Array of documents
	// static String Docs[] = { "C:\\Users\\10637913\\Desktop\\Testing\\words.txt",
	// "C:\\Users\\10637913\\Desktop\\Testing\\words2.txt", };
	List<String> Docs = new ArrayList<>();

	TreeMap<String, Integer> frequencyData = new TreeMap<String, Integer>();
	Hashtable<String, List<Boolean>> invertyedIndexLists = new Hashtable<String, List<Boolean>>();
	Map<String, Map<String, Integer>> wordToDocumentMap = new TreeMap<String, Map<String, Integer>>();
	Map<String, Map<String, Boolean>> invertedIndex = new HashMap<String, Map<String, Boolean>>();

	public Map<String, Map<String, Integer>> getWordToDocumentMap() {
		return wordToDocumentMap;
	}

	public int getCount(String word, TreeMap<String, Integer> frequencyData) {
		if (frequencyData.containsKey(word)) { // The word has occurred before, so get its count from the map
			return frequencyData.get(word); // Auto-unboxed
		} else { // No occurrences of this word
			return 0;
		}
	}

	private List<String> getAllFilesinFolder(String Directory) throws Exception {
		if (Directory == null) {
			throw new Exception("Null Directory Path");
		}
		File f = new File(Directory);

		boolean exists = f.exists(); // Check if the file exists
		boolean isDirectory = f.isDirectory(); // Check if it's a directory

		if (!exists || !isDirectory) {
			throw new Exception("Directory Does not Exists or Provided Path id not Directory.");
		}

		File[] listOfFiles = null;
		try {
			File folder = new File(Directory);
			listOfFiles = folder.listFiles();
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<String> fileListString = new ArrayList<String>();

		// for (int i = 0; i < listOfFiles.length; i++) {
		// if (listOfFiles[i].isFile()) {
		// System.out.println("File " + listOfFiles[i].getName());
		// } else if (listOfFiles[i].isDirectory()) {
		// System.out.println("Directory " + listOfFiles[i].getName());
		// }
		// }

		for (File file : listOfFiles) {
			fileListString.add(file.getAbsolutePath());
		}
		return fileListString;
	};

	public void printAllCounts(TreeMap<String, Integer> frequencyData,
			Map<String, Map<String, Integer>> wordToDocumentMap) {
		System.out.println("-----------------------------------------------");
		System.out.println("    Occurrences    Word");

		for (String word : frequencyData.keySet()) {
			System.out.printf("%15d    %s\n", frequencyData.get(word), word);
		}

		System.out.println("-----------------------------------------------");

		for (Map.Entry<String, Map<String, Integer>> wordToDocument : wordToDocumentMap.entrySet()) {
			String currentWord = wordToDocument.getKey();
			Map<String, Integer> documentToWordCount = wordToDocument.getValue();
			for (Map.Entry<String, Integer> documentToFrequency : documentToWordCount.entrySet()) {
				String document = documentToFrequency.getKey();
				Integer wordCount = documentToFrequency.getValue();
				System.out.println("Word " + currentWord + " found " + wordCount + " times in document " + document);
			}
		}
	}

	public Map<String, Double> search(String query,CosineSimmilarity cosine) {
		String processedQuery = query.trim().toLowerCase();
		String[] splitedQuery = processedQuery.split("\\s+");

		List<List<Boolean>> listofBools = new ArrayList<List<Boolean>>();
		for (String word : splitedQuery) {

			// if a word staarts with "-" then invert booleans as to exclude it from the
			// search list
			boolean negate = false;
			if (word.startsWith("-")) {
				word = word.substring(1);
				negate = true;
			}
			if (invertyedIndexLists.containsKey(word)) {
				List<Boolean> b = invertyedIndexLists.get(word);
				if (negate) {
					for (Boolean boo : b) {
						boo = !boo;
					}
				}
				listofBools.add(b);
			}
		}

		List<Boolean> resultBools = new ArrayList<Boolean>();
		List<Boolean> currentBools = (listofBools != null && listofBools.size() > 0) ? listofBools.get(0)
				: new ArrayList<Boolean>();
		for (int i = 1; i < listofBools.size(); i++) {
			List<Boolean> nextBools = listofBools.get(i);
			for (int j = 0; j < currentBools.size(); j++) {
				Boolean b = (currentBools.get(j) && nextBools.get(j));
				resultBools.add(b);
			}
			currentBools.clear();
			currentBools.addAll(resultBools);
			resultBools.clear();
		}

		List<String> resultDocs = new ArrayList<String>();
		for (int i = 0; i < currentBools.size(); i++) {
			if (currentBools.get(i)) {
				resultDocs.add(Docs.get(i));
			}
		}

		// return Sorting.getFrequency(query, resultDocs, wordToDocumentMap);
		Map<String, Double> fileRank = cosine.calcCosine(Docs, query);
		return fileRank;

	}

	public void readWordFile(String basePath) throws Exception {
		Docs = getAllFilesinFolder(basePath);

		int total = 0;
		Scanner wordFile;
		String word; // A word read from the file
		Integer count; // The number of occurrences of the word
		int counter = 0;
		int docs = 0;

		// **FOR LOOP TO READ THE DOCUMENTS**
		for (int x = 0; x < Docs.size(); x++) { // start of for loop [*
			String docName = Docs.get(x);
			try {
				wordFile = new Scanner(new FileReader(docName));
			} catch (FileNotFoundException e) {
				System.err.println(e);
				return;
			}

			while (wordFile.hasNext()) {
				// Read the next word and get rid of the end-of-line marker if needed:
				word = wordFile.next();

				// This makes the Word lower case.
				word = word.toLowerCase();

				word = word.replaceAll("[^a-zA-Z0-9\\s]", "");

				// Get the current count of this word, add one, and then store the new count:
				count = getCount(word, frequencyData) + 1;
				frequencyData.put(word, count);

				total = total + count;
				counter++;
				docs = x + 1;

				Map<String, Integer> documentToCountMap = wordToDocumentMap.get(word);
				if (documentToCountMap == null) {
					// This word has not been found anywhere before,
					// so create a Map to hold document-map counts.
					documentToCountMap = new TreeMap<>();
					wordToDocumentMap.put(word, documentToCountMap);
				}
				Integer currentCount = documentToCountMap.get(docName);
				if (currentCount == null) {
					// This word has not been found in this document before, so
					// set the initial count to zero.
					currentCount = 0;
				}
				documentToCountMap.put(docName, currentCount + 1);
			}

		} // End of for loop *]
		System.out.println("There are " + total + " terms in the collection.");
		System.out.println("There are " + counter + " unique terms in the collection.");
		System.out.println("There are " + docs + " documents in the collection.");

	}

	public void generateInvertedIndex() {

		// build invertedIndex
		for (Map.Entry<String, Map<String, Integer>> wordToDocument : wordToDocumentMap.entrySet()) {
			String currentWord = wordToDocument.getKey();
			Map<String, Boolean> invertedIndDocMap = invertedIndex.get(currentWord);
			if (invertedIndDocMap == null) {
				invertedIndDocMap = new HashMap<String, Boolean>();
			}
			Map<String, Integer> documentToWordCount = wordToDocument.getValue();
			for (String document : Docs) {
				Integer freq = documentToWordCount.get(document);

				invertedIndDocMap.put(document, (freq != null && freq > 0));
				invertedIndex.put(currentWord, invertedIndDocMap);
			}
		}

		// make table of word to vector of document booleans
		Hashtable<String, List<Boolean>> bools = new Hashtable<String, List<Boolean>>();

		for (Map.Entry<String, Map<String, Boolean>> wordToDocumentBool : invertedIndex.entrySet()) {
			String currentWord = wordToDocumentBool.getKey();
			Map<String, Boolean> invertedIndDocMap = invertedIndex.get(currentWord);

			ArrayList<Boolean> currWordBools = new ArrayList<Boolean>();

			for (int i = 0; i < Docs.size(); i++) {
				String documentName = Docs.get(i);
				Boolean boolVal = invertedIndDocMap.get(documentName);
				currWordBools.add(boolVal);
			}
			bools.put(currentWord, currWordBools);
		}

		invertyedIndexLists = bools;

		// print invertedIndex
		// for (Map.Entry<String, Map<String, Boolean>> wordToDocument :
		// invertedIndex.entrySet()) {
		// String currentWord = wordToDocument.getKey();
		// Map<String, Boolean> documentToWordCount = wordToDocument.getValue();
		// for (Map.Entry<String, Boolean> documentToFrequency :
		// documentToWordCount.entrySet()) {
		// String document = documentToFrequency.getKey();
		// Boolean isPresent = documentToFrequency.getValue();
		// System.out.println(currentWord+ " - "+document+" "+isPresent);
		// }
		// }
	}
}

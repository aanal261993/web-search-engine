package com.alfa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import com.alfa.InvertedIndex.InvInd;
import com.alfa.cosine.CosineSimmilarity;
import com.alfa.webcrawler.WebCrawler;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * Servlet implementation class SearchAppServ
 */
@WebServlet("/SearchAppServ")
public class SearchAppServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ServletConfig config;

	public static int pageCount = -1;
	public static int maxPageDownload;
	boolean shouldCrawl;
	boolean shouldGenerateInvInd;
	boolean shouldRegenerateVectorspace;
	String serializeCrawlFile;
	String serializeInvertedIndFile;
	String serializeVectorSpaceFile;

	static String startURL;
	static String basePath;
	Map<String, String> urlToPageMapping;
	InvInd invertedIndex;
	WebCrawler wCrawl;
	CosineSimmilarity cos;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchAppServ() {
		super();

	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
		super.init(config);
		maxPageDownload = getIntParamValueFromWebXML("maxPageDownload");
		shouldCrawl = getBooleanParamValueFromWebXML("shouldCrawl");
		shouldGenerateInvInd = getBooleanParamValueFromWebXML("shouldGenerateInvIndex");
		shouldRegenerateVectorspace = getBooleanParamValueFromWebXML("shouldRegenerateVectorspace");
		basePath = getStringParamValueFromWebXML("basePath");
		if (basePath == null) {
			basePath = "CrawlData\\";
		}
		startURL = getStringParamValueFromWebXML("startURL");
		if (startURL == null) {
			startURL = "https://en.wikipedia.org/wiki/Mahatma_Gandhi";
		}
		serializeCrawlFile = getStringParamValueFromWebXML("serializeCrawlFile");
		if (serializeCrawlFile == null) {
			serializeCrawlFile = "Crawlit.ser";
		}
		serializeInvertedIndFile = getStringParamValueFromWebXML("serializeInvertedIndFile");
		if (serializeInvertedIndFile == null) {
			serializeInvertedIndFile = "InvertedIndexKryo.ser";
		}
		serializeVectorSpaceFile = getStringParamValueFromWebXML("serializeVectorSpaceFile");
		if (serializeVectorSpaceFile == null) {
			serializeVectorSpaceFile = "VectorSpace.ser";
		}

		if (shouldCrawl) {
			wCrawl = crawlWeb();
		} else {
			try {
				wCrawl = deserializeCrawl(serializeCrawlFile);
			} catch (Exception ex) {
				System.out.println("Excception deserializing crawled data: " + ex.getMessage());
				System.out.println("Regenerating Crawled data...");
				wCrawl = crawlWeb();
			}
		}

		urlToPageMapping = wCrawl.urlToPageMapping;

		invertedIndex = new InvInd();
		if (shouldGenerateInvInd) {
			try {
				generateInverteedIndex();
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			try {
				invertedIndex = deserializeInvertedIndexKryo(serializeInvertedIndFile);
			} catch (Exception ex) {
				System.out.println("Excception deserializing inverted index data: " + ex.getMessage());
				System.out.println("Regenerating Inverted index...");
				try {
					generateInverteedIndex();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		if (shouldRegenerateVectorspace) {
			generateVectorSpace();
		} else {
			try {
				cos = deserializeVectorSpaceKryo(serializeVectorSpaceFile);
			} catch (Exception ex) {
				System.out.println("Exception deserializing Vector Space " + ex.getMessage());
				System.out.println("Regenerating Vector Space...");
				try {
					generateVectorSpace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void generateVectorSpace() {
		System.out.println("Generating Vector Space...");
		cos = new CosineSimmilarity(basePath);
		serializeVectorSpaceKryo(serializeVectorSpaceFile, cos);
	}

	private void generateInverteedIndex() throws Exception {
		System.out.println("Generating Inverted index...");
		invertedIndex.readWordFile(basePath);
		invertedIndex.generateInvertedIndex();
		serializeInvertedIndexKryo(serializeInvertedIndFile, invertedIndex);
	}

	private WebCrawler crawlWeb() {
		System.out.println("Generating Crawled data...");
		WebCrawler wCrawl;
		File f = new File(basePath);
		if (!f.exists()) {
			f.mkdirs();
		}
		File[] existingFiles = f.listFiles();
		if (existingFiles.length > 0) {
			System.out.println("Files Already Exists in crawiling destination. Deleting all !!!");
			for (File file : existingFiles) {
				file.delete();
			}
		}
		wCrawl = new WebCrawler();
		wCrawl.processPage(startURL, basePath);
		serializeCrawl(serializeCrawlFile, wCrawl);
		return wCrawl;
	}

	private String getStringParamValueFromWebXML(String param) {
		String returnValue = getServletContext().getInitParameter(param);
		if(returnValue==null) {
			System.out.println(param+" not found in Web.xml.");
		}
		return returnValue;
	}

	private int getIntParamValueFromWebXML(String param) {
		String paramStringValue = getStringParamValueFromWebXML(param);
		int defaultValueIfFailed = 0;
		if (paramStringValue == null) {
			System.out.println("Setting default value.");
			return defaultValueIfFailed;
		}
		try {
			return Integer.parseInt(paramStringValue);
		} catch (NumberFormatException e) {
			System.out.println(param+" not not parsed correctly. Setting default value.");
			return defaultValueIfFailed;
		}
	}

	private boolean getBooleanParamValueFromWebXML(String param) {
		String paramStringValue = getStringParamValueFromWebXML(param);
		if (paramStringValue == null) {
			System.out.println(param+" Setting default value.");
			return false;
		}
		return paramStringValue.equalsIgnoreCase("true") ? true : false;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String query = request.getParameter("searchQuery");
		System.out.println("Query: " + query);
		if (query.equals(null) || query.equals("")) {
			response.getWriter().append("{}");
			return;
		}

		Map<String, Double> fileRank = invertedIndex.search(query, cos);
		LinkedHashMap<Double, String> output = mapResultsToLinks(urlToPageMapping, fileRank);
		ObjectMapper mapper = new ObjectMapper();
		String jsonResponse = mapper.writeValueAsString(output);

		response.getWriter().append(jsonResponse);
	}

	private static LinkedHashMap<Double, String> mapResultsToLinks(Map<String, String> urlToPageMapping,
			Map<String, Double> fileRank) {
		LinkedHashMap<Double, String> results = new LinkedHashMap<Double, String>();
		for (Entry<String, Double> fr : fileRank.entrySet()) {
			String doc = fr.getKey();
			Double freq = fr.getValue();
			results.put(freq, urlToPageMapping.get(doc.substring(doc.lastIndexOf('\\') + 1)));
		}
		return results;
	}

	static boolean serializeCrawl(String filename, WebCrawler wc) {
		System.out.println("Serializing Crawl data....");
		// Serialization
		try (Output output = new Output(new FileOutputStream(filename))) {
			Kryo kryo = new Kryo();
			kryo.writeClassAndObject(output, wc);
			System.out.println("Serialized...");
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to Serialize...");
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	static WebCrawler deserializeCrawl(String filename) throws FileNotFoundException {
		System.out.println("Deserializing Crawled Data...");
		WebCrawler wc = null;
		try (Input input = new Input(new FileInputStream(filename))) {
			Kryo kryo = new Kryo();
			wc = (WebCrawler) kryo.readClassAndObject(input);

			System.out.println("Deserailized...");
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to Deserailize...");
			throw ex;
		}
		return wc;

	}

	static boolean serializeVectorSpaceKryo(String filename, CosineSimmilarity cos) {
		System.out.println("Serializing Vector Space...");
		try (Output output = new Output(new FileOutputStream(filename))) {
			Kryo kryo = new Kryo();
			kryo.writeClassAndObject(output, cos);
			System.out.println("Serialized...");
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to Serialize...");
			ex.printStackTrace();
			return false;
		}

		return true;
	}

	static CosineSimmilarity deserializeVectorSpaceKryo(String filename) throws Exception {
		System.out.println("Deserializing Vector Space...");
		CosineSimmilarity cos = null;
		try (Input input = new Input(new FileInputStream(filename))) {
			Kryo kryo = new Kryo();
			cos = (CosineSimmilarity) kryo.readClassAndObject(input);
			System.out.println("Deserailized...");
		} catch (Exception ex) {
			System.out.println("Unable to deserialize...");
			throw ex;
		}
		return cos;

	}

	static boolean serializeInvertedIndexKryo(String filename, InvInd ii) {
		System.out.println("Serializing Inverted Index...");
		try (Output output = new Output(new FileOutputStream(filename))) {
			Kryo kryo = new Kryo();
			kryo.writeClassAndObject(output, ii);
			System.out.println("Serialized...");
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to Serialize...");
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	static InvInd deserializeInvertedIndexKryo(String filename) throws Exception {
		System.out.println("Deserializing Inverted Index...");
		InvInd ii = null;
		try (Input input = new Input(new FileInputStream(filename))) {
			Kryo kryo = new Kryo();
			ii = (InvInd) kryo.readClassAndObject(input);
			System.out.println("Deerialized...");
		} catch (Exception ex) {
			System.out.println("Unable to derialize...");
			throw ex;
		}
		return ii;

	}

}

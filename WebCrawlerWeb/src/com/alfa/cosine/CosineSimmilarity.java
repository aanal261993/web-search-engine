package com.alfa.cosine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.alfa.sorting.Sorting;

public class CosineSimmilarity {
	static TreeMap<String, TreeMap<String, Integer>> VectorSpace = new TreeMap<String, TreeMap<String, Integer>>();

	/*
	 * public static void main(String[] args) { File f = new
	 * File("C:\\Users\\lenovo\\Desktop\\Testing\\");
	 * 
	 * File[] existingFiles = f.listFiles(); List<String> fPath = new ArrayList<>();
	 * for (File file : existingFiles) { fPath.add(file.getAbsolutePath()); } // for
	 * (File file : existingFiles) { // double cos = calcCosine(
	 * file.getAbsolutePath() , "Mohandas Karamchand Gandhi"); //
	 * System.out.println(cos + " " +file.getName()); // } Map<String, Double>
	 * fileRank = calcCosine(fPath, "Mohandas Karamchand Gandhi");
	 * 
	 * for (Entry<String, Double> fr : fileRank.entrySet()) {
	 * System.out.println(fr.getValue()+" "+fr.getKey()); } }
	 */
	public CosineSimmilarity(String fileDirPath) {
		VectorSpace = generateVectorSpaceForAllFiles(fileDirPath);
	}

	public Map<String, Double> calcCosine(List<String> Docs, String query) {
		Map<String, Double> fileRank = new TreeMap<String, Double>();
		if (query == null || query.trim().isEmpty()) {
			return fileRank;
		}

		TreeMap<String, Integer> frequencyData_Query = new TreeMap<>();
		for (String qWord : query.trim().toLowerCase().replaceAll("[^a-zA-Z0-9\\s]", "").split(" ")) {
			if (qWord.startsWith("-")) {
				continue;
			}
			if (qWord.length() < 4) {
				continue;
			}
			Integer count = getCount(qWord, frequencyData_Query) + 1;
			frequencyData_Query.put(qWord, count);
		}
		for (String document : Docs) {
			TreeMap<String, Integer> vector = VectorSpace.get(document);
			double cosineValue = 0;
			if (vector != null) {
				cosineValue = cosineSimmiliraty(vector, frequencyData_Query);
			}
			fileRank.put(document, cosineValue);
		}

		return Sorting.sortMapByValuesDouble(fileRank);
	}

	private static TreeMap<String, TreeMap<String, Integer>> generateVectorSpaceForAllFiles(String fileDirPath) {
		// String fileContents =
		// Files.lines(Paths.get(fileName)).collect(Collectors.joining("\n"));
		java.io.File dumpFilesDir = new java.io.File(fileDirPath);
		if (!dumpFilesDir.isDirectory()) {
			System.out.println(fileDirPath + " directory does not exists...");
		}

		java.io.File[] listOfFiles = dumpFilesDir.listFiles();
		List<String> allFiles = new ArrayList<String>();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				allFiles.add(listOfFiles[i].getAbsolutePath());
			}
		}

		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		FileReader fr = null;
		TreeMap<String, TreeMap<String, Integer>> vectorSpace = new TreeMap<String, TreeMap<String, Integer>>();
		for (String fileName : allFiles) {
			TreeMap<String, Integer> frequencyData_File = new TreeMap<String, Integer>();
			try {
				// br = new BufferedReader(new FileReader(FILENAME));
				fr = new FileReader(fileName);
				br = new BufferedReader(fr);

				String sCurrentLine;

				while ((sCurrentLine = br.readLine()) != null) {
					sb.append(sCurrentLine);
				}

				String fileContents = sb.toString().trim().toLowerCase().replaceAll("[^a-zA-Z0-9\\s]", "");
				if (fileContents != null && !fileContents.isEmpty()) {
					List<String> words = Arrays.asList(fileContents.split(" "));
					for (String w : words) {
						if (w.length() < 4) {
							continue;
						}
						Integer count = getCount(w, frequencyData_File) + 1;
						frequencyData_File.put(w, count);
					}
				}
				vectorSpace.put(fileName, frequencyData_File);
			} catch (Exception ex) {
				System.out.println("Exception reading data from: " + fileName);
				System.out.println(ex.getMessage());
			} finally {

				try {
					if (br != null)
						br.close();
					if (fr != null)
						fr.close();
					sb.delete(0, sb.length());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return vectorSpace;
	}

	private static double dotProduct(final Map<String, Integer> m1, final Map<String, Integer> m2) {

		// Loop over the smallest map
		Map<String, Integer> smallerMap = m2;
		Map<String, Integer> largerMap = m1;
		if (m1.size() < m2.size()) {
			smallerMap = m1;
			largerMap = m2;
		}

		double result = 0;
		for (Map.Entry<String, Integer> entry : smallerMap.entrySet()) {
			Integer i = largerMap.get(entry.getKey());
			if (i == null) {
				continue;
			}
			result += 1.0 * entry.getValue() * i;
		}

		return result;
	}

	private static double getNormalVector(final Map<String, Integer> m) {
		double result = 0;

		for (Map.Entry<String, Integer> entry : m.entrySet()) {
			result += 1.0 * entry.getValue() * entry.getValue();
		}

		return Math.sqrt(result);
	}

	static double cosineSimmiliraty(Map<String, Integer> m1, Map<String, Integer> m2) {
		return dotProduct(m1, m2) / (getNormalVector(m1) * getNormalVector(m2));
	}

	static int getCount(String word, Map<String, Integer> frequencyData) {
		if (frequencyData.containsKey(word)) { // The word has occurred before, so get its count from the map
			return frequencyData.get(word); // Auto-unboxed
		} else { // No occurrences of this word
			return 0;
		}
	}
}

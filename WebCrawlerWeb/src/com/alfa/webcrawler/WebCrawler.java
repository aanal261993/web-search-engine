
package com.alfa.webcrawler;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Hello world!
 *
 */
public class WebCrawler implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static int pageCount = -1;
	private static int maxPageDownload = 1000;
	List<String> URL_List = new ArrayList<String>();

	public Map<String, String> urlToPageMapping = new HashMap<String, String>();

	public void processPage(String URL, String basePath) {
		System.out.println("Crawling...");
		List<String> urlToBeProcessed = new ArrayList<>();
		urlToBeProcessed.add(URL);

		for (pageCount = 0; pageCount < maxPageDownload; pageCount++) {

			String currentURL = urlToBeProcessed.get(pageCount);
			urlToBeProcessed.remove(currentURL);
			// urlToBeProcessed.remove(currentURL);
			// check if the given URL is already in database
			if (URL_List.contains(currentURL)) {
				continue;
			}
			System.out.println(currentURL);
			URL_List.add(currentURL);
			Document doc = null;

			try {
				doc = Jsoup.connect(currentURL).get();
			} catch (IOException e) {
				continue;
			}

			String textData = Jsoup.parse(doc.html()).text();
			UUID uuid = UUID.randomUUID();
			String randomUUIDString = uuid.toString() + ".txt";

			urlToPageMapping.put(randomUUIDString, currentURL);
			writeToFile(textData, basePath + randomUUIDString);

			// get all links and recursively call the processPage method
			Elements questions = doc.select("a[href]");
			for (Element link : questions) {
				String nextURL = link.attr("abs:href");
				if (checkExclusions(nextURL)) {
					continue;
				}

				if (nextURL.contains("#")) {
					nextURL = nextURL.substring(0, nextURL.indexOf("#"));
				}

				if (nextURL != null) {
					urlToBeProcessed.add(nextURL);
				}
			}
		}
	}

	private boolean checkExclusions(String nextURL) {
		List<String> exclusions = Arrays.asList(".jpg", ".css", ".js", ".png", ".ico", ".svg", ".mp3", ".mp4", ".mov",
				".ogg", ".oga", ".pdf");
		for (String exclude : exclusions) {
			if (nextURL.toLowerCase().endsWith(exclude)) {
				return true;
			}

		}
		return false;
	}

	private boolean writeToFile(String content, String FILENAME) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {

			bw.write(content);
			// no need to close it.
			// bw.close();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}	
}
